﻿using System;
using System.Collections.Generic;

namespace CSharpEssentials
{
    public class Student
    {
        public int ID;
        public string FirstName;
        public string LastName;
        public int Age;
        public Student(int iD, string firstName, string lastName): this(iD, firstName, lastName, 0)
    {
    }
        public Student(int iD, string firstName, string lastName, int age)
        {
            FirstName = firstName.ToLower();
            LastName = lastName.ToLower();
            Age = age;
            ID = iD;
        }
    }

    public class School
    {
        public string Name;
        public List<Student> Students;

        public School(string name)
        {
            Name = name;
            Students = new List<Student>();
        }

        public void PrintStudents()
        {
            if (Students.Count == 0)
            {
                Console.WriteLine($"В школе {Name} пока нет учеников!\n");
            }
            else
            {
                Console.WriteLine("{0, -2} {1, -10} {2, -10} {3, -10}", "ID", "Имя", "Фамилия","Возраст");
                for (int i = 0; i < Students.Count; i++)
                {
                    Console.WriteLine("{0, -2} {1, -10} {2, -10} {3, -10}", Students[i].ID,
                    Students[i].FirstName.Remove(0,1).Insert(0, Convert.ToString(Students[i].FirstName[0]).ToUpper()),
                    Students[i].LastName.Remove(0, 1).Insert(0, Convert.ToString(Students[i].LastName[0]).ToUpper()), 
                    Students[i].Age == 0? "не указан": Convert.ToString(Students[i].Age));
                    if (i == Students.Count - 1) Console.WriteLine();
                }
            }
        }

        public void AddNewStudent(Student student)
        {
            Students.Add(student);
            Console.WriteLine($"Студент {student.FirstName.Remove(0, 1).Insert(0, Convert.ToString(student.FirstName[0]).ToUpper())} успешно добавлен в школу {Name}.\n");
        }
        public void RemoveNewStudent(string informationForRemove)
        {
            if (Students.Count == 0)
            {
                Console.WriteLine($"В школе {Name} пока нет учеников!\n");
            }
            else
            {
                string[] informationForRemoveMatrix = informationForRemove.Split();
                bool flag = true;
                string saveName = "";
                bool f = int.TryParse(informationForRemoveMatrix[0], out int number);
                if (f == true)
                {
                    for (int i = 0; i < Students.Count; i++)
                    {
                        if (Students[i].ID == Convert.ToInt32(informationForRemove))
                        {
                            string x = Students[i].FirstName.Remove(0, 1).Insert(0, Convert.ToString(Students[i].FirstName[0]).ToUpper());
                            string y = Students[i].LastName.Remove(0, 1).Insert(0, Convert.ToString(Students[i].LastName[0]).ToUpper());
                            saveName = x + " " + y;
                            Students.Remove(Students[i]);
                            flag = false;
                            break;
                        }
                    }

                }
                else if (f == false && informationForRemoveMatrix.Length >= 2)
                {
                    for (int i = 0; i < Students.Count; i++)
                    {
                        if (Students[i].FirstName == informationForRemoveMatrix[0] && Students[i].LastName == informationForRemoveMatrix[1] ||
                            Students[i].FirstName == informationForRemoveMatrix[1] && Students[i].LastName == informationForRemoveMatrix[0])
                        {
                            string x = Students[i].FirstName.Remove(0, 1).Insert(0, Convert.ToString(Students[i].FirstName[0]).ToUpper());
                            string y = Students[i].LastName.Remove(0, 1).Insert(0, Convert.ToString(Students[i].LastName[0]).ToUpper());
                            saveName = x + " " + y;
                            Students.Remove(Students[i]);
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag == true) Console.WriteLine("Данного номера не существует");
                else Console.WriteLine($"Студент {saveName} успешно удалён из списка школы {Name}.\n");
            }
        }
    }

    class Program
    {
        static void Main()
        {
            Console.WriteLine("Здравствуйте! Введите название вашей школы");
            string schoolName = Console.ReadLine();
            School school = new School(schoolName);
            int iD = 1;
            bool ifOneObjectExists = false;
            Console.WriteLine($"Школа {school.Name} успешна создана");

            while (true)
            {
                Console.WriteLine($"Хотите посмотреть список учеников школы {school.Name}? Введите 1. ");
                Console.WriteLine($"Хотите добавить нового ученика в школу {school.Name}? Введите 2. ");
                Console.WriteLine($"Хотите исключить ученика школы {school.Name}? Введите 3.");
                string userAnswer = Console.ReadLine().Trim();
                if (userAnswer == "1")
                {
                    school.PrintStudents();
                }
                else if (userAnswer == "2")
                {
                    bool flag = true;
                    while (flag == true)
                    {
                        Console.WriteLine($"Введите имя, фамилию и возраст (необязательно) ученика через пробел. Чтобы вернутся назад введите выйти");
                        string[] informationAboutNewStudents = Console.ReadLine().Split();
                        if (informationAboutNewStudents[0].ToLower() == "выйти") break;
                        else
                        {
                            int length = informationAboutNewStudents.Length;
                            if (length == 1)
                            {
                                Console.WriteLine("Необходимо указать фамилию");
                                break;
                            }
                            else if (length == 2)
                            {
                                string firstName = informationAboutNewStudents[0];
                                string lastName = informationAboutNewStudents[1];
                                Student student = new Student(iD, firstName, lastName);
                                school.AddNewStudent(student);
                                iD++;
                                flag = false;
                                ifOneObjectExists = true;
                                break;
                            }
                            else
                            {
                                while (true)
                                {
                                    string firstName = informationAboutNewStudents[0];
                                    string lastName = informationAboutNewStudents[1];
                                    string ageString = informationAboutNewStudents[2];
                                    bool ageResult = int.TryParse(informationAboutNewStudents[2], out int result);
                                    if (ageResult == true)
                                    {
                                        int age = Convert.ToInt32(ageString);
                                        if (age >= 0 && age < 130)
                                        {
                                            if (age < 5 && age >= 0)
                                            {
                                                Console.WriteLine("Вундеркинд - здорово!");
                                            }
                                            Student student = new Student(iD, firstName, lastName, age);
                                            school.AddNewStudent(student);
                                            iD++;
                                            flag = false;
                                            ifOneObjectExists = true;
                                            break;
                                        }
                                        else if (age < 0)
                                        {
                                            ageResult = false;
                                        }
                                    }
                                    if (ageResult == false)
                                    {
                                        Console.WriteLine("Неверный ввод возраста.Укажите возраст или введите \"нет\", чтобы вернуться к добавлению ученика\n" +
                                            "Чтобы выйти введите \"выйти\"");
                                        string answerAboutAge = Console.ReadLine().ToLower();
                                        if (answerAboutAge == "нет" || answerAboutAge == "не") break;
                                        else if (answerAboutAge == "выйти")
                                        {
                                            flag = false;
                                            break;
                                        }
                                        else informationAboutNewStudents[2] = answerAboutAge;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (userAnswer == "3")
                {
                    if (ifOneObjectExists == true)
                    {
                        Console.WriteLine("Выберите из списка:");
                        school.PrintStudents();
                        Console.WriteLine("Укажите ID или имя и фамилию через пробел.");
                        string userAnswerAboutRemove = Console.ReadLine().ToLower();
                        school.RemoveNewStudent(userAnswerAboutRemove);
                    }
                    else
                    {
                        school.PrintStudents();
                    }
                }
                else
                {
                    Console.WriteLine("Неверная команда, повтори, Пупсик.");
                }
            }
        }
    }
}